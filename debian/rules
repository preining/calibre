#!/usr/bin/make -f

SETUP=python2.7 setup.py

# build hardening
DPKG_EXPORT_BUILDFLAGS = 1
export DEB_BUILD_MAINT_OPTIONS=hardening=+relro
include /usr/share/dpkg/buildflags.mk

# This filters out *.so for the package "calibre". With this, we can
# install usr/lib in calibre.install and just leave out the binary
# bits.
DEB_DH_INSTALL_ARGS = -Xusr/etc/bash_completion.d $(if $(findstring -bin,$(cdbs_curpkg)),,-X*.so) --fail-missing
DEB_DH_GENCONTROL_ARGS = -- -Vpyqt:Version=$(shell dpkg-query -f'$${Version}' -W python-pyqt5)

export QT_SELECT=qt5

-include /usr/share/cdbs/1/rules/debhelper.mk

common-build-arch::
	[ -d build ] || $(SETUP) build

common-install-arch common-install-indep:: debian/tmp
	dh_buildinfo
	dh_sip

debian/tmp:
	mkdir -p  debian/tmp/usr/share/icons/hicolor
	mkdir -p  debian/tmp/usr/share/applications
	mkdir -p  debian/tmp/usr/share/mime/packages
	env -u LANG -u LC_ALL -u LANGUAGE -u LC_MESSAGES XDG_DATA_DIRS=$(CURDIR)/debian/tmp/usr/share CALIBRE_CONFIG_DIRECTORY=debian/tmp/config $(SETUP) install --root=debian/tmp/usr

	rm debian/tmp/usr/lib/calibre/calibre/web/feeds/feedparser.py
	find debian/tmp/ -name '*.py' | xargs sed -i 's/calibre.utils.pyparsing/pyparsing/g'
	find debian/tmp/ -name '*.py' -o -name "markdown-calibre" | xargs sed -i 's/calibre.ebooks.markdown/markdown/g'

	# not needed
	rm debian/tmp/usr/bin/calibre-uninstall
	rm debian/tmp/usr/share/calibre/calibre-portable.*
	rm debian/tmp/usr/lib/python*/site-packages/init_calibre.py

	# do not ship a copy of fonts-liberation
	rm -r debian/tmp/usr/share/calibre/fonts/liberation/
	ln -s /usr/share/fonts/truetype/liberation/ debian/tmp/usr/share/calibre/fonts/liberation

	# use system mathjax
	rm -rf debian/tmp/usr/share/calibre/viewer/mathjax
	ln -s /usr/share/javascript/mathjax debian/tmp/usr/share/calibre/viewer/mathjax

	# use system coffee-script
	rm -rf debian/tmp/usr/share/calibre/coffee-script.js
	ln -s /usr/share/javascript/coffeescript/coffee-script.min.js debian/tmp/usr/share/calibre/coffee-script.js

	# fix executable permissions
	find debian/tmp/usr/lib/calibre/calibre -type f | xargs chmod a-x
	find debian/tmp/usr/share/calibre/images -type f | xargs chmod a-x

	# fix "env python2" style hashbangs
	find debian/tmp -type f | xargs sed -i '1 { /^#!.*python/ s_^.*$$_#!/usr/bin/python2.7_ }'

install/calibre::
	# Create and install some of the man pages
	dh_installman man-pages/man1/*.1
	for i in man-pages/?? man-pages/??_?? ; do ll=`basename $$i`; dh_installman --language $$ll $$i/man1/*.1 ; done

clean::
	rm -rf debian/orig
	[ ! -d build ] || $(SETUP) build -c
	find . -name *.pyc -delete
	find . -name *.so -delete
	find . -name *.qrc -delete

# Downloads the current upstream release according to debian/changelog, removes
# some bundled software copies, non-free image and replaces the non-free prs500
# TTFs with symlinks to their free liberation fonts counterparts. This also
# updates the unpacked source tree to the new upstream version and adds the
# original non-minimized javascript source for some minified JavaScript.
get-orig-source:
	set -e; \
	V=`dpkg-parsechangelog | sed -rn '/^Version:/ {s/^Version: ([0-9.]+).*$$/\1/; p}'`; \
	mkdir -p debian/orig; cd debian/orig; \
	wget -O - https://download.calibre-ebook.com/$$V/calibre-$$V.tar.xz | xz -cd | tar x; \
	\
	D=`pwd`; rm -f calibre*/src/odf/thumbnail.py; \
	rm -r calibre*/resources/mathjax; \
	rm -r calibre*/resources/coffee-script.js; \
	rm -r calibre*/src/calibre/ebooks/markdown; \
	cd $$D; tar c * | xz -9 > ../../../calibre_$$V+dfsg.orig.tar.xz; \
	cd ../..; rm -r debian/orig
